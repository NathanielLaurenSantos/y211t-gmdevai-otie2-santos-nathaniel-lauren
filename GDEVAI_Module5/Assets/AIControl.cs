using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIControl : MonoBehaviour
{

    NavMeshAgent agent;

    public GameObject target;

    public WASDMovement playerMvmt;

    Vector3 wanderTarget;
    // Start is called before the first frame update  
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        playerMvmt = target.GetComponent<WASDMovement>();
    }

    void Seek(Vector3 location) {
        agent.SetDestination(location);
    }

    void Flee(Vector3 location) {
        Vector3 fleeDirection = location - this.transform.position;
        agent.SetDestination(this.transform.position - fleeDirection);
    }

    void Pursue() {
        Vector3 targetDirection = target.transform.position - this.transform.position;
        float lookAhead = targetDirection.magnitude / (agent.speed + playerMvmt.currentSpeed);
        Seek(target.transform.position + target.transform.forward * lookAhead);
    }

    void Evade() {
        Vector3 targetDirection = target.transform.position - this.transform.position;
        float lookAhead = targetDirection.magnitude / (agent.speed + playerMvmt.currentSpeed);
        Flee(target.transform.position + target.transform.forward * lookAhead);

    }

    
    void Wander() {
        float wanderRadius = 20;
        float wanderDistance = 10;
        float wanderJitter = 1;

        wanderTarget +=new Vector3(Random.Range(-1.0f, 1.0f) * wanderJitter,
                                0,
                                Random.Range(-1.0f, 1.0f) * wanderJitter);
        wanderTarget.Normalize();
        wanderTarget *= wanderRadius;

        Vector3 targetLocal = wanderTarget + new Vector3(0, 0, wanderDistance);
        Vector3 targetWorld = this.gameObject.transform.InverseTransformVector(targetLocal);

        Seek(targetWorld);
    }

    void Hide() {
        float distance = Mathf.Infinity;
        Vector3 chosenSpot = Vector3.zero;

        int hidingSpotsCount = World.Instance.GetHidingSpots().Length;

        for (int i = 0; i < hidingSpotsCount; i++) {
            
            Vector3 hideDirection = World.Instance.GetHidingSpots()[i].transform.position - target.transform.position;
            Vector3 hidingPosition = World.Instance.GetHidingSpots()[i].transform.position + hideDirection.normalized * 5;

            float spotDistance = Vector3.Distance(this.transform.position, hidingPosition);
            if (spotDistance < distance) {
                chosenSpot = hidingPosition;
                distance = spotDistance;
            }

        }
        Seek(chosenSpot);
    }

    // Update is called once per frame
    void Update()
    {
        Hide();
    }
}
