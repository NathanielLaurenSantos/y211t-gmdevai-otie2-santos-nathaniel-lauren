using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TankAI : MonoBehaviour
{
    GameObject[] waypoints;

    Animator animator;

    public NavMeshAgent agent;

    public GameObject player;

    public GameObject bullet;

    public GameObject turret;


    public float currentHealth = 100;
    public GameObject GetPlayer() {
        return player;
    }

    public NavMeshAgent GetAgent() {
        return agent;
    }
    // Start is called before the first frame update
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        animator = this.GetComponent<Animator>();
        //waypoints = GameObject.FindGameObjectsWithTag("waypoint");
        //agent.SetDestination(waypoints[Random.Range(0, waypoints.Length)].transform.position);        
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetFloat("distance", Vector3.Distance(transform.position, player.transform.position));
    }

    void Fire() {
        GameObject b = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
        b.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);
    }

    public void StopFiring() {
        CancelInvoke("Fire");
    }

    public void StartFiring() {
        InvokeRepeating("Fire", 0.5f, 0.5f);
    }

    public void TakeDamage(float damage) {

        currentHealth -= damage;
        animator.SetFloat("health", currentHealth);
        if (currentHealth <= 0) {
            Destroy(this.gameObject);
        }
    }
}
