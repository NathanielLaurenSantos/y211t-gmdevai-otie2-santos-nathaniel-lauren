using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : NPCBaseFSM
{
     override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int latyerIndex)
    {
        base.OnStateEnter(animator, stateInfo, latyerIndex);
        NPC.GetComponent<TankAI>().StartFiring();
    }

    public override void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        NPC.transform.LookAt(Opponent.transform.position);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        NPC.GetComponent<TankAI>().StopFiring();
    }
}
