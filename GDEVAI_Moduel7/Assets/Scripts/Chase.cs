using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chase : NPCBaseFSM
{

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int latyerIndex)
    {
        base.OnStateEnter(animator, stateInfo, latyerIndex);
        agent.ResetPath();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
       
        agent.SetDestination(Opponent.transform.position);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        agent.ResetPath();
    }
}
