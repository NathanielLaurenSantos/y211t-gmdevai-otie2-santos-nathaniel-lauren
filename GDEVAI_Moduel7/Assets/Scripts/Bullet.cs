﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public GameObject explosion;

	public float damage = 20;
	
	void OnCollisionEnter(Collision col)
    {
    	GameObject e = Instantiate(explosion, this.transform.position, Quaternion.identity);
    	Destroy(e,1.5f);
    	Destroy(this.gameObject);
		GameObject target= col.collider.gameObject;
		if (target.tag == "Tank") {
			target.GetComponent<TankAI>().TakeDamage(damage);
		}
		if (target.tag == "Player")
		{
			target.GetComponent<Drive>().TakeDamage(damage);
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
