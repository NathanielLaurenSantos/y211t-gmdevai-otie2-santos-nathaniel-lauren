using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCBaseFSM : StateMachineBehaviour
{

    public GameObject NPC;

    public GameObject Opponent;

    public NavMeshAgent agent;

    public float speed = 2.0f;

    public float rotationSpeed = 1.0f;

    public float accuracy = 3.0f;
    // Start is called before the first frame update
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int latyerIndex) {
        NPC = animator.gameObject;
        Opponent = NPC.GetComponent<TankAI>().GetPlayer();
        agent = NPC.GetComponent<TankAI>().GetAgent();
    }


}
