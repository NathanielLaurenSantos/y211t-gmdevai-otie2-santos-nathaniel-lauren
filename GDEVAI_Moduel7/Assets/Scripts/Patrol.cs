using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Patrol : NPCBaseFSM
{

    GameObject[] waypoints;


    Animator anim;

    
    int currentPoint;

    private void Awake()
    {
       
        waypoints = GameObject.FindGameObjectsWithTag("waypoint");

    }
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        agent.SetDestination(waypoints[Random.Range(0, waypoints.Length)].transform.position);

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        base.OnStateUpdate(animator, stateInfo, layerIndex);
        if (agent.remainingDistance < 10 ) {
            agent.SetDestination(waypoints[Random.Range(0, waypoints.Length)].transform.position);
        }

        
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        agent.ResetPath();
    }


}
