using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flee : NPCBaseFSM
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int latyerIndex)
    {
        base.OnStateEnter(animator, stateInfo, latyerIndex);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Vector3 fleeDirection = Opponent.transform.position - NPC.transform.position;
        agent.SetDestination(NPC.transform.position - fleeDirection);
        agent.stoppingDistance = 0;
    }
        
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        agent.ResetPath();
    }
}
