using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIControls : MonoBehaviour
{

    GameObject[] goalLocation;

    NavMeshAgent agent;

    Animator animator;

    float speedMultiplier;

    float detectionRadius = 500;
    float fleeRadius = 20;
    // Start is called before the first frame update

    void ResetAgenL() {
        speedMultiplier = Random.Range(0.1f, 1.5f);
        agent.speed = 2 * speedMultiplier;
        agent.angularSpeed = 120;
        animator.SetFloat("speedMultiplier", speedMultiplier);
        animator.SetTrigger("isWalking");
        agent.ResetPath();
    }
    void Start()
    {
        goalLocation = GameObject.FindGameObjectsWithTag("goal");
        agent = this.GetComponent<NavMeshAgent>();
        animator = this.GetComponent<Animator>();
        agent.SetDestination(goalLocation[Random.Range(0, goalLocation.Length)].transform.position);
        animator.SetFloat("wOffset", Random.Range(0.1f, 1.0f));
        ResetAgenL();
    }

    // Update is called once per frame
    void Update()
    {
        //if (agent.remainingDistance < 1) {
        //    agent.SetDestination(goalLocation[Random.Range(0, goalLocation.Length)].transform.position);
        //}
    }

    public void DetectNewObstacle(Vector3 location) {
        if (Vector3.Distance(location, this.transform.position) < detectionRadius) {
            Vector3 fleeDirection = (location - this.transform.position).normalized;
            Vector3 newGoal = this.transform.position + fleeDirection * fleeRadius;

            NavMeshPath path = new NavMeshPath();
            agent.CalculatePath(newGoal, path);

            if (path.status != NavMeshPathStatus.PathInvalid) {
                agent.SetDestination(path.corners[path.corners.Length - 1]);
                animator.SetTrigger("isRunning");
                agent.speed = 10;
                agent.angularSpeed = 500;
            }

        }
    }
}
