using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointFollow : MonoBehaviour
{

    public UnityStandardAssets.Utility.WaypointCircuit circuit;
    int currentpoint = 0;
    public float speed = 4;
    public float rotationSpeed = 3;
    public float accuracy = 1;
    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (circuit.Waypoints.Length == 0) return;

        GameObject currentWaypoint = circuit.Waypoints[currentpoint].gameObject;
        Vector3 looatAtPoint = new Vector3(currentWaypoint.transform.position.x,
                                            this.transform.position.y,
                                            currentWaypoint.transform.position.z);
        Vector3 direction = looatAtPoint - this.transform.position;

        if (direction.magnitude <1.0f) {
            currentpoint++;
            if (currentpoint >= circuit.Waypoints.Length) {
                currentpoint = 0;
            }
        }

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                    Quaternion.LookRotation(direction),
                                                    Time.deltaTime * rotationSpeed);
        this.transform.Translate(0, 0, speed * Time.deltaTime);
    }
}
