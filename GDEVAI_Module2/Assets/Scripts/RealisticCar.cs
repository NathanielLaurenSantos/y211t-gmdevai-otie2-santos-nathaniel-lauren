using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RealisticCar : MonoBehaviour
{
    public Transform target;
    public float speed = 0;
    public float rotationSpeed = 3;
    public float acceleration = 5;
    public float decceleration = 5;
    public float minSpeed = 0;
    public float maxSpeed = 15;
    public float breakAngle = 20;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 looatAtPoint = new Vector3(target.position.x,
                                            this.transform.position.y,
                                            target.position.z);
        Vector3 direction = looatAtPoint - this.transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                    Quaternion.LookRotation(direction),
                                                    Time.deltaTime * rotationSpeed);

        
        if (Vector3.Angle(target.forward, this.transform.forward) > breakAngle && speed > 0.3){
            speed = Mathf.Clamp(speed - (decceleration * Time.deltaTime), minSpeed, maxSpeed);
        }

        else {
            speed = Mathf.Clamp(speed + (acceleration * Time.deltaTime), minSpeed, maxSpeed);
        }
        this.transform.Translate(0, 0, speed);

    }
}
