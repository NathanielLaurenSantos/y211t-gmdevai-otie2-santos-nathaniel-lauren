using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour
{

    Transform target;

    public float speed = 5.0f;

    public float accuracy = 1.0f;

    public float rotationSpeed = 2.0f;

    public int destination = 0;

    public GameObject wpManager;

    GameObject[] wps;

    GameObject currentNode;

    int currentPoint = 0;

    Graph graph;
    // Start is called before the first frame update
    void Start()
    {
        wps = wpManager.GetComponent<WaypoitManager>().waypoints;
        graph = wpManager.GetComponent<WaypoitManager>().graph;
        currentNode = wps[0];
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (graph.getPathLength() == 0 || currentPoint == graph.getPathLength()) {
            return;
        }

        currentNode = graph.getPathPoint(currentPoint);

        if (Vector3.Distance(graph.getPathPoint(currentPoint).transform.position, transform.position) < accuracy) {
            currentPoint++;
        }

        if (currentPoint < graph.getPathLength()) {
            target = graph.getPathPoint(currentPoint).transform;
            Vector3 looatAtTarget = new Vector3(target.position.x, transform.position.y, target.position.z);
            Vector3 direction = looatAtTarget - this.transform.position;
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                    Quaternion.LookRotation(direction),
                                                    Time.deltaTime * rotationSpeed);
            this.transform.Translate(0,0, speed * Time.deltaTime);
        }
    }

    public void GoToDestination() {
        graph.AStar(currentNode, wps[destination]);
        currentPoint = 0;
    }
}
