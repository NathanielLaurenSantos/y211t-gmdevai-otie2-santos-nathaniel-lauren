using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentManager : MonoBehaviour
{

    GameObject[] agents;

    public Transform target;

    // Start is called before the first frame update
    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("AI");
    }

    // Update is called once per frame
    void Update()
    {
       
        foreach (GameObject ai in agents) {
            if (Vector3.Distance(ai.transform.position, target.position) > 1){
                ai.GetComponent<AIControl>().agent.SetDestination(target.position);
            }
        }
         
    }
}
