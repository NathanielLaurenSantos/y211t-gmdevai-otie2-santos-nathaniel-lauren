using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public Transform target;
    float rotationSpeed = 4;
    float mvSpeed = 3;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        
        Vector3 lookAtTarget = new Vector3(target.position.x,
                                          this.transform.position.y,
                                          target.position.z);

        Vector3 direction = lookAtTarget - transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                Quaternion.LookRotation(direction),
                                                Time.deltaTime * rotationSpeed);

        if (Vector3.Distance(lookAtTarget, transform.position) > 1) 
        {
            transform.Translate(0,0, mvSpeed * Time.deltaTime);
        }

    }
}
