using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{

    float mvSpeed = 6;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        Vector3 objectPos = Camera.main.WorldToScreenPoint(this.transform.position);
        mousePos.x = mousePos.x - objectPos.x;
        mousePos.y = mousePos.y - objectPos.y;
        Vector3 lookAtMouse = new Vector3(mousePos.x,
                                          this.transform.position.y,
                                          mousePos.y);
        transform.LookAt(lookAtMouse);

        if (Input.GetKey(KeyCode.W)) {
            transform.Translate(0, 0, mvSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(-1*(mvSpeed * Time.deltaTime), 0,0);
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(0, 0, -1 * (mvSpeed * Time.deltaTime));
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(mvSpeed * Time.deltaTime, 0, 0);
        }

    }
}
